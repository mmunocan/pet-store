# app/home/views.py

import requests

from flask import render_template, flash, redirect, url_for
#from flask_login import login_required
from . import home
from .forms import CustomerForm, PetForm, ShoppingCartForm, OrderForm


@home.route('/')
def homepage():
    """
    Render the homepage template on the / route
    """
    return render_template('home/index.html', title="Pet store")


@home.route('/customer', methods=['GET', 'POST'])
def customer():
    """
    Render the dashboard template on the /customer route
	Add a customer to the database through the registration form
    """
    form = CustomerForm()
    if form.validate_on_submit():
        try:
            status1 = 0
            status2 = 0
            while(status1 != 200 or status2 != 200):
                if(status1 != 200):
                    r1 = requests.post('http://petstorecustomer.appspot.com/create', json = {'address':form.address.data, 'name':form.name.data, 'phone':form.phone.data})
                    status1 = r1.status_code
                if(status2 != 200):
                    r2 = requests.get('http://petstorecustomer.appspot.com/pay/49000/by-customer-with-phone/'+str(form.phone.data))
                    status2 = r2.status_code
                
            flash('Your customer was registered successfully.')
            return redirect(url_for('home.customer'))
        except:
            flash('Your customer was not registered for technical reasons.', 'error')
        

    # load template
    return render_template('home/customer.html', form=form, title="Customers")

@home.route('/pet', methods=['GET', 'POST'])
def pet():
    """
    Render the dashboard template on the /pet route
	Add a pet to the database through the registration form
    """
    form = PetForm()
    if form.validate_on_submit():
        try:
            status1 = 0
            while(status1 != 200):
                r1 = requests.post('http://practiceiv-on-gcloud.appspot.com/products/create', json = {'breed':form.breed.data, 'id':form.id.data, 'image_url':'', 'price':form.price.data, 'specie':form.specie.data, 'stock':form.stock.data})
                status1 = r1.status_code
                
            flash('Your pet was registered successfully.')
            return redirect(url_for('home.pet'))
        except:
            flash('Your pet was not registered for technical reasons.', 'error')

    # load template
    return render_template('home/pet.html', form=form, title="Pets")

@home.route('/shoppingCart', methods=['GET', 'POST'])
def shoppingCart():
    """
    Render the dashboard template on the /shoppingCart route
	Add a shopping cart to the database through the registration form
    """
    form = ShoppingCartForm()
    try:
        r1 = requests.get('http://petstorecustomer.appspot.com/list/all')
        r2 = requests.get('http://practiceiv-on-gcloud.appspot.com/products/fetch')
        if(r1.status_code == 200):
            form.customer.choices = [(customer['phone'], str(customer['name'])+' (credit: $'+str(customer['credit'])+')') for customer in r1.json()]
        if(r2.status_code == 200):
            form.pet.choices = [(pet['id'], str(pet['breed'])+' - '+str(pet['specie'])+' ($'+str(pet['price'])+')') for pet in r2.json()['products']]
    except:
        flash('It is not possible to register a shopping cart for technical reasons.', 'error')
    if form.validate_on_submit():
        try:
            status = 0
            while(status != 200):
                r = requests.get('http://studentestwebapp.azurewebsites.net/api/create/'+str(form.customer.data)+'/'+str(form.pet.data)+'/'+str(form.amount.data)+'/0')
                status = r.status_code
            flash('Your shopping cart was registered successfully.')
            return redirect(url_for('home.shoppingCart'))
        except:
            flash('Your shopping cart was not registered for technical reasons.', 'error')
    # load template
    return render_template('home/shoppingCart.html', form=form, title="Shopping Cart")


@home.route('/order', methods=['GET', 'POST'])
def order():
    """
    Render the dashboard template on the /order route
	Add an order to the database through the registration form
    """
    form = OrderForm()
    try:
        r1 = requests.get('http://petstorecustomer.appspot.com/list/all')
        if(r1.status_code == 200):
            form.customer.choices = [(customer['phone'], str(customer['name'])+' (credit: $'+str(customer['credit'])+')') for customer in r1.json()]
    except:
        flash('It is not possible to register a shopping cart for technical reasons.', 'error')
    if form.validate_on_submit():
        cust = form.customer.data
        try:
            # Ask for the customer's pets
            status = 0
            while(status != 200):
                r = requests.get('http://studentestwebapp.azurewebsites.net/api/list/'+str(cust))
                status = r.status_code
            # Ask for the prices
            total_price = 0
            pets_data = []
            for shoppingCart in r.json()['json_list']:
                id_pet = shoppingCart['id_pet']
                cant = shoppingCart['cant']
                status = 0
                while(status != 200 and status != 404):
                    r1 = requests.get('http://practiceiv-on-gcloud.appspot.com/products/get/id/'+str(id_pet))
                    status = r.status_code
                if(status == 404):
                    flash('Your order was not sent for technical reasons.', 'error')
                    return render_template('home/order.html', form=form, title="Orders")
                price = r1.json()['price']
                stock = r1.json()['stock']
                breed = r1.json()['breed']
                specie = r1.json()['specie']
                item = {'pet_species':specie, 'pet_breed':breed, 'pet_amount':cant, 'pet_price':price}
                pets_data.append(item)
                if(cant > stock):
                    delete_shopping_cart(r.json()['json_list'])
                    flash('There is no stock for '+str(breed)+' - '+str(specie)+'. The shopping cart was deleted.')
                    return redirect(url_for('home.order'))
                else:
                    total_price = total_price + (price * cant)
                
            
            # Ask for the customer
            status = 0
            while(status != 200):
                r2 = requests.get('http://petstorecustomer.appspot.com/list/byphone/'+str(cust))
                status = r2.status_code
            
            if(len(r2.json()) == 0):
                flash('Your order was not sent for technical reasons.', 'error')
                return render_template('home/order.html', form=form, title="Orders")
            
            customer_selected = r2.json()[0]
            order_data = {'name':customer_selected['name'], 'address':customer_selected['address'], 'phone':customer_selected['phone'], 'pets': pets_data}
            
            
            credit = customer_selected['credit']
            
            if(total_price > credit):
                delete_shopping_cart(r.json()['json_list'])
                flash('You do not have enought credit. '+str(total_price)+' > '+str(credit)+'. The shopping cart was deleted.')
                return redirect(url_for('home.order'))
            else:
                # Here update the changes
                update_customer_credit(cust, total_price)
                update_pets_stock(r.json()['json_list'])
                delete_shopping_cart(r.json()['json_list'])
                status = 0
                while(status != 200):
                    r3 = requests.post('http://petstoreorder.appspot.com/create/order', json = order_data)
                    status = r3.status_code
                flash('Your order was registered successfully.')
                return redirect(url_for('home.order'))
            
        except:
            flash('Your order was not sent for technical reasons.', 'error')
            

    # load template
    return render_template('home/order.html', form=form, title="Orders")

def update_customer_credit(id_cust, payment):
    status = 0
    while(status != 200):
        r = requests.get('http://petstorecustomer.appspot.com/pay/'+str(int(payment))+'/by-customer-with-phone/'+str(id_cust))
        status = r.status_code
    

    
def update_pets_stock(list_shopping_cart):
    for shoppingCart in list_shopping_cart:
        id_pet = shoppingCart['id_pet']
        cant = shoppingCart['cant']
        status = 0
        while(status != 200 and status != 404):
            r = requests.get('http://practiceiv-on-gcloud.appspot.com/products/get/id/'+str(id_pet))
            status = r.status_code
        if(status == 404):
            return
        price = r.json()['price']
        stock = r.json()['stock']
        breed = r.json()['breed']
        specie = r.json()['specie']
        stock = stock-cant
        status1 = 0
        while(status1 != 200):
            r1 = requests.post('http://practiceiv-on-gcloud.appspot.com/products/update', json = {'id':id_pet, 'stock':stock})
            status1 = r1.status_code
        
        

def delete_shopping_cart(list_shopping_cart):
    for shopping_cart in list_shopping_cart:
        id_pet = shopping_cart['id_pet']
        id_cus = shopping_cart['id_user']
        status = 0
        while(status != 200):
            r = requests.get('http://studentestwebapp.azurewebsites.net/api/delete/'+str(id_cus)+'/'+str(id_pet))
            status = r.status_code
        