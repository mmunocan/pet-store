# app/auth/forms.py

import requests

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, ValidationError, IntegerField, SelectField
from wtforms.validators import DataRequired, Email, EqualTo, NumberRange

class CustomerForm(FlaskForm):
    """
    Form for users to create new Customer
    """
    phone = IntegerField('ID', validators=[DataRequired("This field have to be a number")])
    name = StringField('Name', validators=[DataRequired()])
    address = StringField('Address', validators=[DataRequired()])
    submit = SubmitField('Register')
    
    def validate_phone(self, field):
        phone=field.data
        try:
            r = requests.get('http://petstorecustomer.appspot.com/list/byphone/'+str(phone))
        except:
            raise ValidationError("It was not possible to check the phone number")
            
        if(r.status_code == 200):
            if(len(r.json()) > 0):
                raise ValidationError("This phone number is registered")
        else:
            raise ValidationError("It was not possible to check the phone number")
   
   
class PetForm(FlaskForm):
    """
    Form for users to create new Pet
    """
    id = IntegerField('ID', validators=[DataRequired("This field have to be a number")])
    specie = StringField('Specie', validators=[DataRequired()])
    breed = StringField('Breed', validators=[DataRequired()])
    price = IntegerField('Price', validators=[DataRequired("This field have to be a number"), NumberRange(min=1)])
    stock = IntegerField('Stock', validators=[DataRequired("This field have to be a number"), NumberRange(min=0)])
    submit = SubmitField('Register')
    def validate_id(self, field):
        id=field.data
        try:
            r = requests.get('http://practiceiv-on-gcloud.appspot.com/products/get/id/'+str(id))
        except:
            raise ValidationError("It was not possible to check the customer's id")
            
        if(r.status_code == 200):
            if(len(r.json()) > 0):
                raise ValidationError("This id is registered")
        if(r.status_code != 404):
            raise ValidationError("It was not possible to check the customer's id")
    
class ShoppingCartForm(FlaskForm):
    """
    Form for users to create new shopping cart
    """ 
    customer = SelectField('Choose a Customer', choices = [])
    pet = SelectField('Choose a Pet', choices = [])
    amount = IntegerField('Amount', validators=[DataRequired("This field have to be a number"), NumberRange(min=1)])
    submit = SubmitField('Add a shopping cart')
    
    def validate_customer(self, field):
        try:
            cust = field.data
            pet = self.pet.data
            status = 0
            while(status != 200):
                r = requests.get('http://studentestwebapp.azurewebsites.net/api/list/'+str(cust))
                status = r.status_code
        except:
            raise ValidationError('It was not possible to check the relation customer/pet in the shopping cart register.')
            
        for pets in r.json()['json_list']:
            if(str(pets['id_pet']) == str(pet)):
                raise ValidationError('The relation customer/pet exist in the shopping cart register.')

        
    
class OrderForm(FlaskForm):
    """
    Form for users to create new order
    """
    customer = SelectField('Choose a Customer', choices = [])
    submit = SubmitField('Send order')
    
    def validate_customer(self, field):
        try:
            cust = field.data
            status = 0
            while(status != 200):
                r = requests.get('http://studentestwebapp.azurewebsites.net/api/list/'+str(cust))
                status = r.status_code
        except:
            raise ValidationError("It was not possible to check the customer's shopping cart.")
        
        if(len(r.json()['json_list']) == 0):
            raise ValidationError('The customer selected does not have any shopping cart.')